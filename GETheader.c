#include "GETheader.h"
#include "server.h"
#include "RESPheader.h"
#include <stdio.h>
#include <string.h>




int parse_GET_request(char* message, struct GET_header* header){
	char workingcopy[BUFFERSIZE];
	char delimeter[6] = " \r\n\t\v";

	strcpy(workingcopy, message);

	char *get; 

	if((get = strtok(workingcopy, delimeter))==NULL){//there is anything
		perror("NOT VALID");
		header->status = BADREQUEST;
		return -1;
	}

	if (strcmp(get, "GET") != 0)//check if it has a get
	{
		perror("NOT VALID");
		header->status = BADREQUEST;
		return -1;
	}

	char *temp;
	if((temp = strtok(NULL, delimeter)) == NULL){ //checks for file path
		perror("NOT VALID");
		header->status = FILENOTFOUND;
		return -1;
	}
	memset(header->filepath, 0, PATHLEN);
	strcat(header->filepath, root);
	strcat(header->filepath, temp);
	//saves the file path



	if((temp = strtok(NULL, delimeter)) == NULL){
		perror("NOT VALID");
		header->status = BADREQUEST;
		return -1;
	}

	strcpy(header->httpversion, temp);

}
