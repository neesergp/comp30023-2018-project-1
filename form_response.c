#include "RESPheader.h"
#include "GETheader.h"
#include "server.h"

const char* html = "Content-Type: text/html\n\n";
const char* htmlext = ".html";
const char* javascript = "Content-Type: application/javascript\n\n";
const char* jsext = ".js";
const char* jpeg = "Content-Type: image/jpeg\n\n";
const char* jpegext= ".jpg";
const char* css = "Content-Type: text/css\n\n";
const char* cssext= ".css";


int formresponse(char buffer[BUFFERSIZE], struct RESP_header* header){
	strcat(buffer, header->httpversion);
	strcat(buffer, " ");

	if(header->status == SUCCESS){//form responce header if it is valid
		char status[4];
		sprintf(status, "%d", header->status);
		strcat(buffer, status);
		strcat(buffer, " OK\n");
		char content_length[30];
		sprintf(content_length, "Content-Length: %d\n", header->content_length);
		strcat(buffer, content_length);
		strcat(buffer, header->contenttype);
		return 0;

	}

	else if (header->status ==FILENOTFOUND)// 404 response header
	{
		char status[4];
		sprintf(status, "%d", header->status);
		strcat(buffer, status);
		strcat(buffer, " Not Found\n\n");
		return 1;
	}

	else{ // other response header
		char status[4];
		sprintf(status, "%d", header->status);
		strcat(buffer, status);
		strcat(buffer, " Error\n\n");
	}

	return 2;
} 


int sendresponse(int sockfd, struct GET_header* header){
	FILE *fp;
	fp = fopen(header->filepath, "r");
	struct RESP_header resphead;
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	if (fp == NULL)
	{
		strcpy(resphead.httpversion, header->httpversion);
		resphead.status = FILENOTFOUND;
		formresponse(buffer, &resphead);
		return send(sockfd, buffer, strlen(buffer), 0);
	}// sends the 404 response

	else{
		resphead.status = SUCCESS;
		strcpy(resphead.httpversion, header->httpversion);

		fseek(fp, 0, SEEK_END);
		int size = ftell(fp);
		fseek(fp, 0,SEEK_SET);
		resphead.content_length = size;
		if(getfiletype(header->filepath, &resphead)!=0){
			formresponse(buffer, &resphead);
			fclose(fp);
			return send(sockfd, buffer,strlen(buffer), 0);
		}
		formresponse(buffer, &resphead);

		if(send(sockfd, buffer,strlen(buffer), 0)<0){
			return -1;	
			}
			//sends the desired file
	while(!feof(fp)){
		memset(buffer, 0, BUFFERSIZE);
		int n_bytes = fread(buffer, 1, BUFFERSIZE, fp);
		if(n_bytes <=0){
		break;
		}
		if(send(sockfd, buffer,n_bytes,0)<0){
			fclose(fp);
			return -1;	
		}
		
	} 
	fclose(fp);
	return 0;

	}
	fclose(fp);
	return -1;
}

//determining mime type
int getfiletype(const char *filepath, struct RESP_header* resphead){
	if (strstr(filepath, htmlext) != NULL)
	{
		strcpy(resphead->contenttype, html);
		return 0;
	}
	else if (strstr(filepath, jsext) != NULL)
	{

		strcpy(resphead->contenttype, javascript);
		return 0;
	}

	else if (strstr(filepath, cssext) != NULL)
	{
		strcpy(resphead->contenttype, css);
		return 0;
	}

	else if (strstr(filepath, jpegext) != NULL)
	{
		strcpy(resphead->contenttype, jpeg);
		return 0;
	}

	else{
		resphead-> status = 400;
		return -1;
	}
}





