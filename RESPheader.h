#ifndef RESPheader
#define RESPheader 

#include "GETheader.h"

#define CONTENTTYPESIZE 40
struct RESP_header{
	char httpversion[10]; 
	int status;
	char contenttype [40];
	int content_length;
};
int sendresponse(int sockfd, struct GET_header* header);
int getfiletype(const char *filepath, struct RESP_header* resphead);





#endif