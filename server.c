# include <pthread.h>
#include "server.h"
#include "GETheader.h"
#include "RESPheader.h"

#define MAXUSER 5

void* handleclient(void * newsockfd);

int main(int argc, char *argv[]){

	struct addrinfo info, *res, *p;//struct to hold all the server adress info
	int sockfd, newsockfd;//socket number to hold socket ids
	struct sockaddr_storage clien_addr;//holds client adress info
	socklen_t clien_size; //size of clien adress
	


	if (argc <3){
		fprintf(stderr,"ERROR, no port provided\n");//to check if there is no port provided at start
		exit(1);
	}
	strcpy(root, argv[2]);// sets root directory
	memset(&info, 0, sizeof(info));




	info.ai_family = AF_UNSPEC; ///sets it to unspecified
	info.ai_socktype = SOCK_STREAM; //TCP connection stream oriented
	info.ai_flags = AI_PASSIVE; //listen on on all ip interface of machine
	info.ai_protocol = 0; //set this to zero as default

	if((getaddrinfo(NULL, argv[1], &info, &res) != 0))// gets a lnked list of adress infos
	{
		fprintf(stderr,"ERROR, no port provided\n");//to check if getaddrinfo gets any addrinfos
		exit(1);
	}

	//loops through the linked list for a connection
	for (p = res; p!= NULL; p = p->ai_next)
	{
		if((sockfd = socket(p-> ai_family, p->ai_socktype, p->ai_protocol)) < 0){
			perror("ERROR opening socket");
			continue;
		}

		if ((bind(sockfd, p->ai_addr, p->ai_addrlen)) < 0){

			perror("ERROR on binding");
			continue;
		}

		break;
	}

	if (p == NULL)//if no connection established
	{
		perror("ERROR on creating and binding");
		return 2;
	}

	if(listen(sockfd,MAXUSER)<0){ //listens on that socket
		perror("ERROR listening");
		return 2;
}	


	while(1){
		clien_size = sizeof (clien_addr);
		//accepts connections, opening a new scoket for sending and recieving

		if((newsockfd = accept(sockfd, (struct sockaddr *) &clien_addr, &clien_size)) < 0)
		{
			perror("ERROR accepting");
			return 3;
		}

		pthread_t tid;

		if (pthread_create(&tid, NULL, handleclient, &newsockfd) <0)//create a worker thread
		{
			perror("ERROR creating thread");
			return 3;
		}

		pthread_detach(tid);// free all the resource associated with thread

	}



	 return 0;



}


void* handleclient(void * newsockfd){
	int *sockfd_ptr = (int*) newsockfd;
	int numbytes;
	int sockfd = *sockfd_ptr;
	char buffer[BUFFERSIZE];
	bzero(buffer, BUFFERSIZE);

	//recieve the message
	if ((numbytes = recv(sockfd, buffer, BUFFERSIZE, 0)) <0)
	{
		perror("ERROR recieving");
		close(sockfd);
		return NULL;
	}
	struct GET_header header;
	//parse the message
	parse_GET_request(buffer, &header);

	//send the appropriate message according to get response
	if (sendresponse(sockfd, &header) <0)
	{
		perror("ERROR sending");
		close(sockfd);
		return NULL;
	}

	close(sockfd);
}

