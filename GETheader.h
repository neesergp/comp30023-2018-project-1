#ifndef GETheader
#define GETheader

#define BADREQUEST 400
#define FILENOTFOUND 404
#define PATHLEN 128
#define SUCCESS 200

struct GET_header
{
	char httpversion[10];
	int status;
	char filepath[PATHLEN];

};


int parse_GET_request(char* message, struct GET_header* header);







#endif
